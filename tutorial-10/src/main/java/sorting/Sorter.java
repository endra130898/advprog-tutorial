package sorting;


public class Sorter {

    /**
     * Some sorting algorithm that possibly the slowest algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */

    public static int[] slowSort(int[] inputArr) {
        int temp;
        for (int i = 1; i < inputArr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (inputArr[j] < inputArr[j - 1]) {
                    temp = inputArr[j];
                    inputArr[j] = inputArr[j - 1];
                    inputArr[j - 1] = temp;
                }
            }
        }
        return inputArr;
    }

    /**
     * Entry point for quick sort algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @return a sorted array of integer.
     */
    public static int[] fastSort(int[] inputArr) {
        return fastSortHelper(inputArr, 0, inputArr.length - 1);
    }

    /**
     * Quick sort algorithm.
     *
     * @param inputArr array of integer that need to be sorted.
     * @param low lower bound index to be sorted.
     * @param high upper bound index to be sorted.
     * @return a sorted array of integer.
     */
    private static int[] fastSortHelper(int[] inputArr, int low, int high) {
        if (low < high) {
            int mid = low - 1;
            int pivot = inputArr[high];
            int temp;

            for (int i = low; i < high; i++) {
                if (inputArr[i] < pivot) {
                    mid++;
                    temp = inputArr[mid];
                    inputArr[mid] = inputArr[i];
                    inputArr[i] = temp;
                }
            }
            temp = inputArr[mid + 1];
            inputArr[mid + 1] = inputArr[high];
            inputArr[high] = temp;
            mid++;

            fastSortHelper(inputArr, low, mid - 1);
            fastSortHelper(inputArr, mid + 1, high);
        }

        return inputArr;
    }
}
