package sorting;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SortAndSearchTest {

    private static int[] unorderedNumbers;
    private static int[] orderedNumbers;

    @Before
    public void setUp() {
        unorderedNumbers = new int[] {100, 5, 20, 13, 98, 8, 12, 1, 322, 1000, 3, 2, 4};
        orderedNumbers = new int[] {1, 2, 3, 4, 5, 8, 12, 13, 20, 98, 100, 322, 1000};
    }

    @Test
    public void slowSortTest() {
        int[] sortedNumbers = Sorter.slowSort(unorderedNumbers);
        for (int i = 0; i < sortedNumbers.length; i++) {
            assertEquals(orderedNumbers[i], sortedNumbers[i]);
        }
    }

    @Test
    public void fastSortTest() {
        int[] sortedNumbers = Sorter.fastSort(unorderedNumbers);
        for (int i = 0; i < sortedNumbers.length; i++) {
            assertEquals(orderedNumbers[i], sortedNumbers[i]);
        }
    }

    @Test
    public void slowSearchExistTest() {
        assertEquals(13, Finder.slowSearch(unorderedNumbers, 13));
    }

    @Test
    public void slowSearchNotExistTest() {
        assertEquals(-1, Finder.slowSearch(unorderedNumbers, 31));
    }

    @Test
    public void fastSearchExistTest() {
        int[] sortedNumbers = Sorter.fastSort(unorderedNumbers);
        for (int i = 0; i < sortedNumbers.length; i++) {
            assertEquals(orderedNumbers[i], sortedNumbers[i]);
        }
        assertEquals(13, Finder.fastSearch(sortedNumbers, 13));
    }

    @Test
    public void fastSearchNotExistTest() {
        int[] sortedNumbers = Sorter.fastSort(unorderedNumbers);
        for (int i = 0; i < sortedNumbers.length; i++) {
            assertEquals(orderedNumbers[i], sortedNumbers[i]);
        }
        assertEquals(-1, Finder.fastSearch(sortedNumbers, 31));
    }
}
