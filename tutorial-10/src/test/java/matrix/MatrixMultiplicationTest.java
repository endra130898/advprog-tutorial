package matrix;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MatrixMultiplicationTest {

    private static double[][] firstMatrix;
    private static double[][] secondMatrix;
    private static double[][] resultMatrix;

    @Before
    public void setUp() {
        firstMatrix = new double[][] {
            new double[] {1, 2},
            new double[] {4, 5}
        };
        secondMatrix = new double[][] {
            new double[] {6, 5},
            new double[] {3, 0},
        };
        resultMatrix = new double[][] {
            new double[] {12, 5},
            new double[] {39, 20}
        };
    }

    @Test
    public void basicMultiplicationAlgorithmTest()
        throws InvalidMatrixSizeForMultiplicationException {
        double[][] actualResult = MatrixOperation.basicMultiplicationAlgorithm(firstMatrix,
                                                                               secondMatrix);
        for (int i = 0; i < actualResult.length; i++) {
            for (int j = 0; j < actualResult[i].length; j++) {
                assertEquals(resultMatrix[i][j], actualResult[i][j], 0.000001);
            }
        }
    }

    @Test
    public void strassenMatrixMultiForNonSquareMatrixTest()
        throws InvalidMatrixSizeForMultiplicationException {
        double[][] actualResult = MatrixOperation
            .strassenMatrixMultiForNonSquareMatrix(firstMatrix, secondMatrix);
        for (int i = 0; i < actualResult.length; i++) {
            for (int j = 0; j < actualResult[i].length; j++) {
                assertEquals(resultMatrix[i][j], actualResult[i][j], 0.000001);
            }
        }
    }
}
