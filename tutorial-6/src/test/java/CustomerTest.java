import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CustomerTest {

    private Customer customer;
    private Movie movie;
    private Movie movie2;
    private Movie movie3;
    private Movie movie4;
    private Rental rent;
    private Rental rent2;
    private Rental rent3;
    private Rental rent4;

    @Before
    public void setUp() {
        customer = new Customer("Alice");

        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);

        movie2 = new Movie("Endrawan's Movie", Movie.NEW_RELEASE);
        rent2 = new Rental(movie2, 365);

        movie3 = new Movie("Advanced Programming", Movie.CHILDREN);
        rent3 = new Rental(movie3, 5);

        movie4 = new Movie("This is a movie?", 3);
        rent4 = new Rental(movie4, 0);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("1 frequent renter points"));
    }

    @Test
    public void statementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);
        customer.addRental(rent4);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(7, lines.length);
        assertTrue(result.contains("Amount owed is 1103.0"));
        assertTrue(result.contains("5 frequent renter points"));
    }

    @Test
    public void htmlStatementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("<br>");

        assertEquals(4, lines.length);
        assertTrue(result.contains("<p>Amount owed is <em>3.5</em></p><br>"));
        assertTrue(result.contains("<em>1</em> frequent renter points"));
    }

    @Test
    public void htmlStatementWithMultipleMovies() {
        customer.addRental(rent);
        customer.addRental(rent2);
        customer.addRental(rent3);
        customer.addRental(rent4);

        String result = customer.htmlStatement();
        String[] lines = result.split("<br>");

        assertEquals(7, lines.length);
        assertTrue(result.contains("<p>Amount owed is <em>1103.0</em></p><br>"));
        assertTrue(result.contains("<em>5</em> frequent renter points"));
    }
}
