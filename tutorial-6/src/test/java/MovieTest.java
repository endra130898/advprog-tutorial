import org.junit.Before;
import org.junit.Test;

import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MovieTest {

    private Movie movie;
    private Movie movie2;

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        movie2 = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
    }

    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Bad Black");

        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(Movie.CHILDREN);

        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void equalsSameObjectTest() {
        assertTrue(movie.equals(movie));
    }

    @Test
    public void equalsDifferentClassTest() {
        assertFalse(movie.equals("Endrawan"));
    }

    @Test
    public void equalsDifferentObjectTest() {
        assertTrue(movie.equals(movie2));
    }

    @Test
    public void hashCodeTest() {
        assertEquals(Objects.hash(movie.getTitle(), movie.getPriceCode()), movie.hashCode());
    }
}
