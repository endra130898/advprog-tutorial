package game;

import java.util.Random;
import java.util.Scanner;

public class QuizProblemGenerator implements Runnable {

    private Score scoreReference;
    private Scanner scannerReference;
    private Thread thread;
    private QuizTimer quizTimerReference;
    private int thresholdTime;

    private static final int TOTAL_QUEST = 10;
    private static final int RIGHT_BELOW_THRESHOLD_POINT = 10;
    private static final int RIGHT_ABOVE_THRESHOLD_POINT = 5;
    private static final int WRONG_POINT = 0;
    private static final int QUEST_TYPE_ADD = 0;
    private static final int QUEST_TYPE_SUBSTR = 1;
    private static final int QUEST_TYPE_MULTIPL = 2;
    private static final int QUEST_TYPE_DIVS = 3;

    public QuizProblemGenerator(Score scoreReference, Scanner scannerReference,
                                int thresholdTime, QuizTimer quizTimerReference) {
        this.scoreReference = scoreReference;
        this.scannerReference = scannerReference;
        this.quizTimerReference = quizTimerReference;
        this.thresholdTime = thresholdTime;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this, "Quiz Problem Generator");
            thread.start();
        }
    }

    @Override
    public void run() {
        for (int questNo = 1; questNo <= TOTAL_QUEST; questNo++) {
            System.out.print(questNo + ") ");
            Random rand = new Random();
            Fraction firstPosFrac = new Fraction(rand.nextInt(40) - 20,
                                                 rand.nextInt(40) - 20);
            Fraction secondPosFrac = new Fraction(rand.nextInt(40) - 20,
                                                  rand.nextInt(40) - 20);
            Fraction expectedAnswer;

            switch (rand.nextInt(3)) {
                case QUEST_TYPE_ADD:
                    System.out.print(firstPosFrac.toString() + "  +  "
                                         + secondPosFrac.toString() + "  =  ");
                    expectedAnswer = firstPosFrac.getAddition(secondPosFrac);
                    break;
                case QUEST_TYPE_SUBSTR:
                    System.out.print(firstPosFrac.toString() + "  -  "
                                         + secondPosFrac.toString() + "  =  ");
                    expectedAnswer = firstPosFrac.getSubstraction(secondPosFrac);
                    break;
                case QUEST_TYPE_MULTIPL:
                    System.out.print(firstPosFrac.toString() + "  *  "
                                         + secondPosFrac.toString() + "  =  ");
                    expectedAnswer = firstPosFrac.getMultiplication(secondPosFrac);
                    break;
                case QUEST_TYPE_DIVS:
                    System.out.print(firstPosFrac.toString() + "  :  "
                                         + secondPosFrac.toString() + "  =  ");
                    expectedAnswer = firstPosFrac.getDivision(secondPosFrac);
                    break;
                default:
                    System.out.println("Oooops!");
                    expectedAnswer = new Fraction();
            }

            // Asking for question
            // And capture before and after the time in milis
            long totalMilis = System.currentTimeMillis();
            String rawAns = scannerReference.nextLine();
            totalMilis = System.currentTimeMillis() - totalMilis;

            // Process user answer
            Fraction userAnswer;
            if (rawAns.contains("/")) {
                String[] ans = rawAns.split("/");
                userAnswer = new Fraction(Integer.parseInt(ans[0]),
                                          Integer.parseInt(ans[1]));
            } else {
                userAnswer = new Fraction(Integer.parseInt(rawAns));
            }

            // Check answer
            if (expectedAnswer.isEqual(userAnswer)) {
                if (totalMilis / 1000 <= thresholdTime) {
                    scoreReference.incrementScore(RIGHT_BELOW_THRESHOLD_POINT);
                } else {
                    scoreReference.incrementScore(RIGHT_ABOVE_THRESHOLD_POINT);
                }
            } else {
                scoreReference.setScore(WRONG_POINT);
            }

            // Output current score and time
            System.out.println("Current score: " + scoreReference.getScore()
                                   + ". Time needed to answer the problem: "
                                   + (double) totalMilis / 1000.0);

            // Update time
            scoreReference.incrementTime(totalMilis);
        }

        // End timer
        quizTimerReference.end();
    }
}
