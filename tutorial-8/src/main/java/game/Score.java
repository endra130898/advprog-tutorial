package game;

public class Score {

    private double score;
    private long timeElapsedInMillis;

    public Score() {
        score = 0;
        timeElapsedInMillis = 0;
    }

    public synchronized void incrementScore(int percentage) {
        score += score * (double) percentage / 100.0;
    }

    public synchronized void decrementScore(double value) {
        this.score -= value;
    }

    public synchronized void incrementTime(long timeElapsedInMillis) {
        this.timeElapsedInMillis += timeElapsedInMillis;
    }

    public synchronized double getScore() {
        return score;
    }

    public synchronized void setScore(double score) {
        this.score = score;
    }

    public synchronized long getTimeElapsedInMillis() {
        return timeElapsedInMillis;
    }

    public synchronized void setTimeElapsedInMillis(long timeElapsedInMillis) {
        this.timeElapsedInMillis = timeElapsedInMillis;
    }
}
