package game;

public class QuizTimer implements Runnable {

    private Score scoreReference;
    private Thread thread;
    private double decrementPoint;
    private boolean endTime;

    public QuizTimer(Score scoreReference, double decrementPoint) {
        this.scoreReference = scoreReference;
        this.decrementPoint = decrementPoint;
        this.endTime = false;
    }

    public void start() {
        if (thread == null) {
            thread = new Thread(this, "Quiz Timer");
            thread.start();
        }
    }

    public void end() {
        this.endTime = true;
    }

    public boolean isEndTime() {
        return endTime;
    }

    @Override
    public void run() {
        try {
            while (true) {
                scoreReference.decrementScore(decrementPoint);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            // empty
        }
    }
}
