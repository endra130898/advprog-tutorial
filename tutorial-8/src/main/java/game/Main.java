package game;

import java.util.Scanner;

/**
 * Created by billy on 9/27/16.
 * Edited by hafiyyan94 on 4/10/18
 */

public class Main {

    private static final int INITIAL_SCORE = 100;
    private static final double DECREMENT_POINT = 1;

    public static void main(String[] args) {
        // write your code here
        Scanner scanner = new Scanner(System.in);
        String startNewQuestsIpt;
        int thresholdTime;
        Score score = new Score();

        do {
            // initialize value
            startNewQuestsIpt = "";
            score.setScore(INITIAL_SCORE);
            score.setTimeElapsedInMillis(0);
            QuizTimer quizTimer = new QuizTimer(score, DECREMENT_POINT);

            // Asking for asnwering question threshold time
            System.out.print("How much time do you need "
                    + "to answer each question? (In second) ");
            String rawInput = scanner.nextLine();
            thresholdTime = rawInput.isEmpty() ? 20 : Integer.parseInt(rawInput);

            QuizProblemGenerator quiz = new QuizProblemGenerator(score, scanner,
                                                                 thresholdTime, quizTimer);
            quizTimer.start();
            quiz.start();

            // stop the main thread
            while (!quizTimer.isEndTime()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // empty
                }
            }

            // Print the result
            System.out.println("\n=========Result==========");
            System.out.println("\nTotal score : " + score.getScore()
                                   + ". Total time needed to answer all the problem : "
                                   + (double) score.getTimeElapsedInMillis() / 1000.0);
            System.out.println("\n");

            // Asking if user want to start a new questions
            // if the respond is not what we want, ask it again and again
            while (!startNewQuestsIpt.equalsIgnoreCase("y")
                    && !startNewQuestsIpt.equalsIgnoreCase("n")) {
                System.out.println("Restart the quiz? [y/n]");
                startNewQuestsIpt = scanner.nextLine();
            }
            System.out.println("\n\n\n\n\n\n");
        } while (startNewQuestsIpt.equalsIgnoreCase("y"));
        // while user input yes, do same step again
    }
}
