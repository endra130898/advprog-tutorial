package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter extends TallyCounter {

    private AtomicInteger counter = new AtomicInteger();

    @Override
    public void increment() {
        counter.getAndIncrement();
    }

    @Override
    public void decrement() {
        counter.getAndDecrement();
    }

    @Override
    public int value() {
        return counter.get();
    }
}
