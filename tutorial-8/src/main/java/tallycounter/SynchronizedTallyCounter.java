package tallycounter;

public class SynchronizedTallyCounter extends TallyCounter {

    @Override
    public synchronized void increment() {
        super.increment();
    }

    @Override
    public synchronized void decrement() {
        super.decrement();
    }

    @Override
    public synchronized int value() {
        return super.value();
    }
}
