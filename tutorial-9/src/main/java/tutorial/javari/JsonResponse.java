package tutorial.javari;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JsonResponse {

    @JsonProperty
    private String message;

    public JsonResponse(String message) {
        this.message = message;
    }
}
