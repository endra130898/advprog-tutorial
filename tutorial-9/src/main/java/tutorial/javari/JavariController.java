package tutorial.javari;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import tutorial.javari.animal.Animal;

@RestController
public class JavariController {

    private JavariRepository javariRepository;

    @Autowired
    public JavariController(JavariRepository javariRepository) {
        this.javariRepository = javariRepository;
    }

    @GetMapping("/javari")
    public Object getAllAnimals() {
        List<Animal> animals = javariRepository.getAnimals();

        if (!animals.isEmpty()) {
            return animals;
        } else {
            return new ErrorResponse();
        }
    }

    @GetMapping("/javari/{id}")
    public Object getAnimal(@PathVariable(value = "id") int id) {
        Optional<Animal> animal = javariRepository.getAnimalById(id);

        if (animal.isPresent()) {
            return animal.get();
        } else {
            return new ErrorResponse();
        }
    }

    @DeleteMapping("/javari/{id}")
    public Object removeAnimal(@PathVariable(value = "id") int id) {
        Optional<Animal> animal = javariRepository.deleteAnimalById(id);

        if (animal.isPresent()) {
            return animal.get();
        } else {
            return new ErrorResponse();
        }
    }

    @PostMapping("/javari")
    public Object addAnimal(@RequestBody Animal animal) {
        javariRepository.addAnimal(animal);
        return new JsonResponse("OK");
    }
}
