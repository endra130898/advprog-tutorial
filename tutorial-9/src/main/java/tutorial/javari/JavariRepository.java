package tutorial.javari;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

@Repository
public class JavariRepository {

    private List<Animal> animals;
    private static final String FILE_NAME = "javari_database.csv";

    @Autowired
    public JavariRepository() {
        animals = new ArrayList<>();
        readDataFromCsv();
    }

    public List<Animal> getAnimals() {
        return animals;
    }

    public Optional<Animal> getAnimalById(Integer id) {
        Optional<Animal> animalById = animals.stream()
                                             .filter(animal -> animal.getId().equals(id))
                                             .findFirst();
        return animalById;
    }

    public Optional<Animal> deleteAnimalById(Integer id) {
        Optional<Animal> animalById = animals.stream()
                                             .filter(animal -> animal.getId().equals(id))
                                             .findFirst();
        if (animalById.isPresent()) {
            animals.remove(animalById);
            writeDataToCsv();
        }

        return animalById;
    }

    public void addAnimal(Animal animal) {
        animals.add(animal);
        writeDataToCsv();
    }

    private void readDataFromCsv() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(FILE_NAME))) {
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] lineSplit = line.split(",");

                Integer id = Integer.parseInt(lineSplit[0]);
                String type = lineSplit[1];
                String name = lineSplit[2];
                String gender = lineSplit[3];
                double length = Double.parseDouble(lineSplit[4]);
                double weight = Double.parseDouble(lineSplit[5]);
                String condition = lineSplit[6];

                animals.add(new Animal(id,
                                       type,
                                       name,
                                       Gender.parseGender(gender),
                                       length,
                                       weight,
                                       Condition.parseCondition(condition)));
            }
        } catch (IOException exception) {
            // empty
        }
    }

    private void writeDataToCsv() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(FILE_NAME))) {
            for (Animal animal : animals) {
                String result = animal.getId() + ","
                    + animal.getType() + ","
                    + animal.getName() + ","
                    + animal.getGender() + ","
                    + animal.getLength() + ","
                    + animal.getWeight() + ","
                    + animal.getCondition() + "\n";

                bufferedWriter.write(result);
            }
        } catch (IOException exception) {
            // empty
        }
    }
}
