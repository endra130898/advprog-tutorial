package applicant;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.util.function.Predicate;

public class ApplicantTest {

    private Applicant applicant;
    private Predicate<Applicant> creditEvaluator;
    private Predicate<Applicant> criminalRecordsEvaluator;
    private Predicate<Applicant> employmentEvaluator;
    private Predicate<Applicant> qualifiedEvaluator;

    @Before
    public void setUp() {
        applicant = new Applicant();
        creditEvaluator = anApplicant -> anApplicant.getCreditScore() > 600;
        criminalRecordsEvaluator = anApplicant -> !anApplicant.hasCriminalRecord();
        employmentEvaluator = anApplicant -> anApplicant.getEmploymentYears() > 0;
        qualifiedEvaluator = Applicant::isCredible;
    }

    @Test
    public void testAcceptedEvaluatingApplicant() {
        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator.and(creditEvaluator)));
        assertTrue(Applicant.evaluate(applicant, qualifiedEvaluator.and(
            employmentEvaluator).and(creditEvaluator)));
    }

    @Test
    public void testRejectedEvaluatingApplicant() {
        assertFalse(Applicant.evaluate(applicant, qualifiedEvaluator.and(
            employmentEvaluator).and(criminalRecordsEvaluator)));
        assertFalse(Applicant.evaluate(applicant, qualifiedEvaluator.and(
            employmentEvaluator).and(creditEvaluator).and(criminalRecordsEvaluator)));
    }
}
