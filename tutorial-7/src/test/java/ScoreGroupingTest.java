import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScoreGroupingTest {

    private Map<String, Integer> scores;

    @Before
    public void setUp() {
        scores = new HashMap<>();
    }

    @Test
    public void testScoreGroupingWithDifferentScore() {
        scores.put("Endrawan", 1);
        scores.put("Andika", 2);
        scores.put("Wicaksana", 3);

        Map<Integer, List<String>> result = ScoreGrouping.groupByScores(scores);

        assertEquals(1, result.get(1).size());
        assertEquals(true, result.get(1).contains("Endrawan"));

        assertEquals(1, result.get(2).size());
        assertEquals(true, result.get(2).contains("Andika"));

        assertEquals(1, result.get(3).size());
        assertEquals(true, result.get(3).contains("Wicaksana"));
    }

    @Test
    public void testScoreGroupingWithSameScore() {
        scores.put("Endrawan", 1);
        scores.put("Andika", 1);
        scores.put("Wicaksana", 2);

        Map<Integer, List<String>> result = ScoreGrouping.groupByScores(scores);

        assertEquals(2, result.get(1).size());
        assertEquals(true, result.get(1).contains("Endrawan"));
        assertEquals(true, result.get(1).contains("Andika"));

        assertEquals(1, result.get(2).size());
        assertEquals(true, result.get(2).contains("Wicaksana"));
    }
}
