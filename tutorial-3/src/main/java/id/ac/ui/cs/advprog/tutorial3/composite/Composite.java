package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;

public class Composite {

    public static void main(String[] args) {
        Company company = new Company();

        company.addEmployee(new Ceo("Endrawan", 1_000_000));
        company.addEmployee(new SecurityExpert("Andika", 100_000));

        company.getAllEmployees().forEach(employees -> {
            System.out.println(employees.getName() + " (" + employees.getRole() + "). Salary: " + employees.getSalary());
        });

        System.out.println("Net salaries: " + company.getNetSalaries());
    }
}
