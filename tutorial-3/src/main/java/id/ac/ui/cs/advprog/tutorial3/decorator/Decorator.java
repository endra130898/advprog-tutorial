package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Decorator {

    public static void main(String[] args) {
        Food food = FillingDecorator.CHEESE.addFillingToBread(
                    FillingDecorator.BEEF_MEAT.addFillingToBread(
                    FillingDecorator.TOMATO_SAUCE.addFillingToBread(
                    BreadProducer.THICK_BUN.createBreadToBeFilled())));

        System.out.println(food.getDescription() + ". cost: " + food.cost());
    }
}
