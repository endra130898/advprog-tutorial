package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class BeefMeat extends Filling {

    Food food;

    public BeefMeat(Food food) {
        this.food = food;
    }

    @Override
    public double cost() {
        return food.cost() + 6;
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding beef meat";
    }
}
