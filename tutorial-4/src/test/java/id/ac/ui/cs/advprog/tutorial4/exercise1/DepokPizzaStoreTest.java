package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import static org.junit.Assert.assertEquals;

public class DepokPizzaStoreTest {

    private PizzaStore pizzaStore;

    @Before
    public void setUp() {
        pizzaStore = new DepokPizzaStore();
    }

    @Test
    public void createCheesePizzaTest() {
        Pizza pizza = pizzaStore.orderPizza("cheese");
        assertEquals("Depok Style Cheese Pizza", pizza.getName());
    }

    @Test
    public void createVeggiePizzaTest() {
        Pizza pizza = pizzaStore.orderPizza("veggie");
        assertEquals("Depok Style Veggie Pizza", pizza.getName());
    }

    @Test
    public void createClamPizzaTest() {
        Pizza pizza = pizzaStore.orderPizza("clam");
        assertEquals("Depok Style Clam Pizza", pizza.getName());
    }
}
