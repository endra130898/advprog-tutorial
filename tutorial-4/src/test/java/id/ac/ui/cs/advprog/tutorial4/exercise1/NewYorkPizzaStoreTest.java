package id.ac.ui.cs.advprog.tutorial4.exercise1;

import org.junit.Before;
import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import static org.junit.Assert.assertEquals;

public class NewYorkPizzaStoreTest {

    private PizzaStore pizzaStore;

    @Before
    public void setUp() {
        pizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void createCheesePizzaTest() {
        Pizza pizza = pizzaStore.orderPizza("cheese");
        assertEquals("New York Style Cheese Pizza", pizza.getName());
    }

    @Test
    public void createVeggiePizzaTest() {
        Pizza pizza = pizzaStore.orderPizza("veggie");
        assertEquals("New York Style Veggie Pizza", pizza.getName());
    }

    @Test
    public void createClamPizzaTest() {
        Pizza pizza = pizzaStore.orderPizza("clam");
        assertEquals("New York Style Clam Pizza", pizza.getName());
    }
}
