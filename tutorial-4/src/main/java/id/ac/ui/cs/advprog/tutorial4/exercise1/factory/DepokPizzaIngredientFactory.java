package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.JuustoleipaCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.HotClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.RavioliDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.BologneseSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Corn;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    @Override
    public Dough createDough() {
        return new RavioliDough();
    }

    @Override
    public Sauce createSauce() {
        return new BologneseSauce();
    }

    @Override
    public Cheese createCheese() {
        return new JuustoleipaCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies[] veggies = { new BlackOlives(), new Corn(), new Eggplant(), new Spinach() };
        return veggies;
    }

    @Override
    public Clams createClam() {
        return new HotClams();
    }
}
