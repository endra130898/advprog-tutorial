package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

public class PizzaTestDrive {

    public static void main(String[] args) {
        PizzaStore nyStore = new NewYorkPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("clam");
        System.out.println("Ethan ordered a " + pizza + "\n");

        pizza = nyStore.orderPizza("veggie");
        System.out.println("Ethan ordered a " + pizza + "\n");

        // Create a new Pizza Store franchise at Depok
        PizzaStore myStore = new DepokPizzaStore();

        pizza = myStore.orderPizza("cheese");
        System.out.println("Endrawan ordered a " + pizza + "\n");

        pizza = myStore.orderPizza("clam");
        System.out.println("Endrawan ordered a " + pizza + "\n");

        pizza = myStore.orderPizza("veggie");
        System.out.println("Endrawan ordered a " + pizza + "\n");
    }
}
