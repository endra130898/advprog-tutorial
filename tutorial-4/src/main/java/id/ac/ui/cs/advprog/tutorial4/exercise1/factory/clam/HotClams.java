package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class HotClams implements Clams {

    @Override
    public String toString() {
        return "Hot Clams from Fasilkom";
    }
}
