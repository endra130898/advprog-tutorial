package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class Corn implements Veggies {

    @Override
    public String toString() {
        return "Corn";
    }
}
